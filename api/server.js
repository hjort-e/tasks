const express     = require('express');
const app         = express();
const http        = require('http').Server(app);
const io          = require('./socketio/io');

const morgan      = require('morgan');
const body_parser = require('body-parser');
const jwt         = require('./middleware/jwt');
const cors        = require('./middleware/cors');

const mongoose = require('mongoose');
const user     = require('./mongoose/user.model');

const token_router = require('./routes/token.router');
const user_router  = require('./routes/user.router');

const log    = require('./util/log');
const config = require('./config');

// Middleware
app.use(morgan('dev'));
app.use(jwt);
app.use(cors);
app.use(body_parser.json());

// Database
mongoose.Promise = global.Promise;
mongoose.connect(config.db, { useNewUrlParser: true })
	.then((db) => {
        log('Mongoose/MongoDB connected', 'info');
	})
	.catch((err) => {
        log({ 'Connection error': err }, 'error');
        process.exit()
	});

// Websocket
io(http);

// Routes
app.use('/token', token_router);
app.use('/user', user_router);

// 404 handler
const FOFhandler = function (req, res) {
    res.status(404).send({ message: 'Not found' });
}

app.get('*', FOFhandler);
app.post('*', FOFhandler);
app.put('*', FOFhandler);
app.delete('*', FOFhandler);

// Start listening
http.listen(config.port, log(
    {
        message: 'API running',
        ENV: config.env,
        Port: config.port
    },
    'info'
));
