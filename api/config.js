const secretConfig = require('./secrets');
const env = process.env['NODE_ENV'].trim();

let config;

if (env === 'development') {
    config = {
        db: 'mongodb://localhost/tasks',
        webapp: 'http://localhost:3000', // For CORS
        port: 8091,
        env: env
    };
} else if (env === 'production') {
    config = {
        db: 'mongodb://mongo:27017/raven',
        webapp: 'https://tasks.hjort.xyz', // For CORS
        port: 80,
        env: env
    };
}

module.exports = {
    ...config,
    ...secretConfig
}
