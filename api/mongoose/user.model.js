const mongoose = require('mongoose');
const schema = mongoose.Schema;
const Task = require('./task.schema');
const bcrypt = require('bcryptjs');
const SALT_WORK_FACTOR = 10;

let user_schema = new schema({
	id: mongoose.Schema.Types.ObjectId,
	username: {
		type: String,
		required: true,
		unique: true,
		lowercase: true,
		maxlength: 128,
	},
	password: {
		type: String,
		required: true,
		minlength: 8,
		maxlength: 64,
		validate: {
			validator: function (v) {
				if (!(/[a-zA-Z]/g.test(v))){
					return false
				} else if (!(/\d+/g.test(v))){
					return false
				} else {
					return true;
				}
			},
			message: 'Must contain 1 letter and 1 number'
		}
    },
    tasks: [Task]
});

user_schema.pre('save', async function (next) {
    // Skip if password has not been altered
    if (!this.isModified('password')) return next();
    
    // Generate salt for hashing
    let salt;
    try {
        salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
    } catch (err) {
        return next(err);
    }
    
    // Hash the password
    let hash;
    try {        
        hash = await bcrypt.hash(this.password, salt);
    } catch (err) {
        return next(err);
    }
    
    // Attach hash to user and procced
    this.password = hash;
    next();
});

user_schema.methods.compare_password = async function (candidate_password) {
    // Compare the passwords and return bool
    let match;
    try {        
        match = await bcrypt.compare(candidate_password, this.password)
        if (!match) throw new Error ('Password do not match')
        else return true;
    } catch (error) {
        return false;
    }
};

user_schema.methods.simplify = function () {
    const user = this;
    return {
        id: user._id,
        username: user.username
    }
};

user_schema.methods.simplify_task = function (task) {
    const { title, description, completed } = task;
    const id = task._id;
    return {
        id,
        title,
        description,
        completed
    };
};

user_schema.methods.simplify_tasks = function () {
    const user = this;
    return user.tasks.map((task) => this.simplify_task(task));
};


module.exports = mongoose.model('User', user_schema);
