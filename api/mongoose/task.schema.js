const mongoose = require('mongoose');
const schema = mongoose.Schema;

let task_schema = new schema({
    id: mongoose.Schema.Types.ObjectId,
    temp_id: {
        type: String,
        required: false,
        maxlength: 64
    },
    title: {
        type: String,
        required: true,
        minlength: 1,
        maxlength: 128
    },
    description: {
        type: String,
        required: false,
        maxlength: 128
    },
    completed: {
        type: Boolean,
        required: true
    }
});

module.exports = task_schema;
