const mongoose = require('mongoose');
const User     = require('./user.model');

mongoose.Promise = global.Promise;
const test_users = [
{
        username: 'hjort-e',
        password: '1234abcd',
        tasks: [
            {
                title: 'Buy milk',
                description: '2 litres',
                completed: true
            },
            {
                title: 'Buy egg',
                completed: false
            },
            {
                title: 'Buy dragon',
                completed: false
            }
        ]
    },
    {
        username: 'rahjo',
        password: '1234abcd',
        tasks: [
            {
                title: 'Buy dogs',
                description: '2 dogs',
                completed: false
            },
            {
                title: 'Buy fish',
                completed: true
            },
            {
                title: 'Buy typewriter',
                completed: true
            }
        ]
    }
];

const log = console.log;
const exit_with_error = function (msg, err) {
    console.error(msg, err);
    log('Exiting...');
    process.exit();
}

async function seed () {
    // Connect to DB
    try {
        await mongoose.connect('mongodb://localhost/tasks')
    } catch (err) {
        exit_with_error('Connection error:', err);
    }
    log('MongoDB is connected...');

    // Clear user collection
    try {
        await User.deleteMany({});
    } catch (err) {
        exit_with_error('Could not clear collection:', err);
    }
    log('Collection cleared...');

    // Insert test users
    const user_promises = [];
    for (user_data of test_users) {
        const user = new User(user_data);
        user_promises.push(user.save());
    }

    try {
        await Promise.all(user_promises);
    } catch (err) {
        exit_with_error('Could not seed all users:', err);
    }
    log('All users successfully seeded...');
    process.exit();
}

seed();
