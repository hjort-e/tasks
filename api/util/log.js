module.exports = function (message, level = 'log') {
    // Parse message based on type for nicer logging
    let parsed_message;
    const message_type = typeof message;

    switch (message_type) {
        case 'string':
            parsed_message = message;
            break;
        case 'object':
            parsed_message = Object.entries(message)
                .map((pair) => {
                    const [key, value] = pair;
                    return `"${key}"="${value.toString()}"`;            
                })
                .join(', ');
            break;
        default:
            parsed_message = JSON.stringify(message);
            break;
    }

    // Get appropriate log method and add some COLOR!!
    let logger;
    let style = '';
    switch (level) {
        case 'info':
            logger = console.info;
            style = '\x1b[32m'; // Green
            break;
        case 'warn':
            logger = console.warn;
            style = '\x1b[33m'; // Yellow
            break;
        case 'error':
            logger = console.error;
            style = '\x1b[31m'; // Red
            break;
        default:
            logger = console.log;
            break;
    }
    
    const style_reset = '\x1b[0m';
    logger(`${style}[Log] ${parsed_message}${style_reset}`);
};
