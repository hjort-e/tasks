const User = require('../../mongoose/user.model');

module.exports = function (socket, user_data) {
    User.findById(user_data.id, 'tasks', function (err, user) {
        if (err || !user) console.log(err || 'No user found')
        else socket.emit('push_tasks', user.simplify_tasks());
    });
}