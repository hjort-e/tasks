const User = require('../../mongoose/user.model');

module.exports = async function ({ socket, user_data }, incomming_task) {
    // Find user
    let user;
    try {
        user = await User.findById(user_data.id);
        if (!user) throw new Error('No user found');
    } catch (error) {
        socket.emit('push_task', { error: 'Could not create task', incomming_task});
        return;
    }
    
    // Find and update task
    let task = user.tasks.id(incomming_task.id);
    delete task._id;
    task.set({
        ...task,
        ...incomming_task
    });

    // Save user
    try {
        await user.save();
    } catch (error) {
        socket.emit('push_task', { error: 'Could not create task', incomming_task});
        return;
    }

    // Reply success
    socket.emit('push_task', user.simplify_task(task));
}