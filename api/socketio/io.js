const socketio = require('socket.io');
const jwt      = require('jsonwebtoken');
const config   = require('../config');
const log      = require('../util/log');

const push_tasks = require('./actions/push_tasks');
const create_task = require('./actions/create_task');
const update_task = require('./actions/update_task');

module.exports = function (http) {
    const io = socketio(http);
    
	io.on('connection', function (socket) {
		jwt.verify(socket.handshake.query.token, config.secret, function (err, user_data) {
			if (err) {
				socket.disconnect(true); // Close connection if theres an error decoding token
				console.log(err);
			} else if (!(user_data.username && user_data.id)) {
				socket.disconnect(true); // Close connection since token is fishy or missing data
			} else {
                console.log('User connected');
                push_tasks(socket, user_data);
                
                socket.on('create_task', (task) => create_task({ socket, user_data }, task));
                socket.on('update_task', (task) => update_task({ socket, user_data }, task));

                socket.on('disconnect', () => {
                    console.log('User disconnected');
                });
			}
		});
	});
};
