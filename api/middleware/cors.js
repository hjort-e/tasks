const cors = require('cors');
const config = require('../config');

module.exports = cors({
	origin: config.webapp,
	methods: 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
	credentials: true,
	allowedHeaders: 'Origin, X-Requested-With, Content-Type, Accept, Authorization, authorization'
});