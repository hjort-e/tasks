const express      = require('express');
const token_router = express.Router();
const mongoose     = require('mongoose');
const jwt          = require('jsonwebtoken');
const User         = mongoose.model('User');
const config       = require('../config');
const log          = require('../util/log')

// POST /token
token_router.post('/', async function(req, res) {
    let { username, password } = req.body;

    // Find user
    let user;
    try {
        user = await User.findOne({ username });
        if (!user) throw new Error('No user found');
    } catch (error) {
        log({ error }, 'error');
        res.status(401).send({ error: 'Authentication failed' });
        return;
    }
    
    // Check that the entered password is correct
    let match = await user.compare_password(password);
    if (!match) return res.status(401).send({ error: 'Authentication failed' });

    // Create and sign JWT
    const token = jwt.sign(
        user.simplify(),
        config.secret,
        {}
    );
    
    // Success response
    res.json({ token });
});

module.exports = token_router;
