const express     = require('express');
const user_router = express.Router();
const mongoose    = require('mongoose');
const User        = mongoose.model('User');

// GET /user
user_router.get('/', async function (req, res) {
    const { username } = req.user;
    
    // Find user
    let user;
    try {
        user = await User.findOne({ username });
        if (!user) throw new Error('No user found');
    } catch (error) {
        log({ error }, 'error');
        res.status(400).send({ error: 'Could not find user' });
        return;
    }
    
    // Success response
    res.json({
        user: user.simplify()
    });
});

// PATCH /user
user_router.patch('/', async function (req, res) {
    const { username } = req.user;
    const { old_password, new_password } = req.body;
    
    // Find user
    let user;
    try {
        user = await User.findOne({ username });
        if (!user) throw new Error('No user found');
    } catch (error) {
        log({ error }, 'error');
        res.status(400).send('Could not find user');
        return;
    }
    
    // Check that old password matches
    let match;
    try {
        match = await user.compare_password(old_password)
        if (!match) throw new error();
    } catch (error) {
        log({ error }, 'error');
        res.status(401).send({ error: 'Authentication failed' });
        return;
    }
    
    // Udate user with new password
    user.password = new_password;
    try {
        await user.save();
    } catch (error) {
        log({ error }, 'error');
        if (err) res.status(500).send({ message: 'Update failed' });
        return;
    }
    
    // Success response
    res.json({
        user: user.simplify()
    });
});

module.exports = user_router;
