const id_filter = (id) => (task) => task.id === id || task.temp_id === id;

export default {
    CREATE_TASK (state, task) {
        state.tasks.push(task);
    },
    UPDATE_TASK (state, incomming_task) {
        // Find and extract
        const task_index = state.tasks.findIndex(id_filter(incomming_task.id || incomming_task.temp_id));
        let task = state.tasks[task_index];

        // Changes
        task = {
            ...task,
            ...incomming_task,
            pending_changes: true
        };

        // Save
        state.tasks.splice(task_index, 1);
        state.tasks.push(task);
    },
    DELETE_TASK (state, id) {
        // Find
        const task_index = state.tasks.findIndex(id_filter(id));
        
        // Delete
        delete state.tasks[task_index];
    },
    OVERWRITE_TASKS (state, tasks) {
        // Replace
        state.tasks = tasks;
    },
    OVERWRITE_TASK (state, incomming_task) {
        // Find and extract
        const task_index = state.tasks.findIndex(id_filter(incomming_task.temp_id));
        let task = state.tasks[task_index];

        // Changes
        task = {
            ...incomming_task,
            pending_changes: false
        };

        // Save
        state.tasks[task_index] = task;
    },
};