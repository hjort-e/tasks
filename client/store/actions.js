const id_filter = (id) => (task) => task.id === id || task.temp_id === id;

export default {
    create_task ({ commit }, { title, description, completed, socket }) {
        // Create task
        let task = {
            temp_id: 'temp_' + Date.now(),
            title,
            description,
            completed,
            pending_changes: true
        };

        // Save to client store
        commit('CREATE_TASK', task);
        
        // Save to server
        socket.emit('create_task', task);
    },
    toggle_task ({ commit,state }, { id, completed, socket }) {
        commit('UPDATE_TASK', { id, completed });

        const task_index = state.tasks.findIndex(id_filter(id));
        const task = state.tasks[task_index];
        socket.emit('update_task', task);
    },
    update_task ({ commit, state }, { id, title, description, socket }) {
        commit('UPDATE_TASK', { id, title, description });

        const task_index = state.tasks.findIndex(id_filter(id));
        const task = state.tasks[task_index];
        socket.emit('update_task', task);
    },
    delete_task ({ commit }, { id }) {
        commit('DELETE_TASK', id);
    },
    socket_pushTasks ({ commit }, tasks) {
        commit('OVERWRITE_TASKS', tasks);
    },
    socket_pushTask ({ commit }, task) {
        console.log(task);
        commit('OVERWRITE_TASK', task);
    }
};