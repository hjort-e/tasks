const colors = require('vuetify/es5/util/colors').default;
const dev = process.env.NODE_ENV !== 'production';

module.exports = {
    /*
    ** Headers of the page
    */
    head: {
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: 'Task/TODO personal assistant' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ]
    },
    /*
    ** CSS paths
    */
    css: [
        'assets/main.css'
    ],
    /*
    ** Nuxt modules
    */
    modules: [
        '@nuxtjs/pwa',
        '@nuxtjs/axios',
        '@nuxtjs/auth',
        '@nuxtjs/vuetify'
    ],
    /*
    ** Nuxt plugins
    */
    plugins: [
        { src: '~/plugins/socketio.js', ssr: false }
    ],
    /*
    ** Nuxt/Vue router
    */
    router: {
        extendRoutes(routes, resolve) {
            routes.push({
                name: '*',
                path: '*',
                component: resolve(__dirname, 'pages/redirect.vue')
            })
        }
    },
    /*
    ** @nuxtjs/auth options
    */
    auth: {
        strategies: {
            local: {
                endpoints: {
                    login: { url: 'token', method: 'post', propertyName: 'token' },
                    user: { url: 'user', method: 'get', propertyName: 'user' },
                    logout: false
                }
            }
        }
    },
    /*
    ** Customize the progress bar color
    */
    loading: { color: colors.yellow.base },
    /*
    ** @nuxtjs/vuetify options
    */
    vuetify: {
        theme: {
            primary: colors.deepOrange.base,
            secondary: colors.deepOrange.darken1,
            accent: colors.orange.base,
            error: colors.red.base,
            warning: colors.yellow.base,
            info: colors.indigo.base,
            success: colors.green.base
        }
    },
    /*
    ** @nuxtjs/axios options
    */
    axios: {
        baseURL: dev ? 'http://localhost:8091' : 'https://api.tasks.hjort.xyz',
        https: !dev
    },
    /*
    ** Global nuxt values
    */
    env: {
        baseUrl: dev ? 'http://localhost:8091' : 'https://api.tasks.hjort.xyz',
    },
    /*
    ** Build configuration
    */
    build: {
        /*
        ** Run ESLint on save
        */
        vendor: [
            '~/node_modules/vue-socket.io-extended',
            '~/node_modules/socket.io-client'
        ],
        extend(config, { isDev, isClient }) {
            if (isDev && isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/
                })
            }
        },
    }
}
