import vue from 'vue'
import vue_socket_io from 'vue-socket.io-extended'
import io from 'socket.io-client';

export default (context) => {
    const { app, store } = context;

    vue.use(
        vue_socket_io,
        io(
            process.env.baseUrl,
            { autoConnect: false, token: null }
        ),
        { store: app.store }
    )
}