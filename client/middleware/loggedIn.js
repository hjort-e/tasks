export default function ({ store, redirect }) {
    console.log('running loggedin middleware');
    if (store.state.auth.loggedIn) {
        return redirect('/dashboard')
    } else {
        return redirect('/login')
    }
}